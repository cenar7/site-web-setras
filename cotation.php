<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="witdh=device-width initial-scase=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--Css local-->
    <link rel="stylesheet" href="stylesheet.css">

    <title>SETRAS CAMEROUN</title>

    <link rel="icon" href="favicon.ico" />
    <link rel="icon" type="image/png" href="Logotype SETRAS.jpg" />

</head>

<body>
<?php include 'header.php'; ?> 
<?php include 'Menu.php'; ?>

    <img src="img4.jpg" class="d-block w-100" height="400">

    <div class="container mt-3 bg-light">
       <p>
            Vous avez un projet ou un investissement à faire dans l’immobilier et vous souhaitez obtenir une Cotation de nos prestations. Envoyez-nous vos Plans, ou tout simplement demander un rendez-vous et nous nous chargerons d’étudier et d’analyser vos besoins
            afin de chiffrer à juste titre et en fonction de votre budget le prix à payer.
        </p>
        <form action="" method="post" class="needs-validation" novalidate>
            <div class="form-group">
                <input type="text" class="form-control" id="uname" placeholder="Nom(s) et prenom(s) " name="Nom" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="uname" placeholder="Entreprise " name="Entreprise" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="uname" placeholder="Fonction " name="Fonction" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <div class="form-group">
                <input type="number" class="form-control" id="uname" placeholder="Téléphone " name="Telephone" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="uname" placeholder="Email " name="Email" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <textarea class="form-control" rows="10" id="comment" placeholder="Entrer votre besoin ici" name="Besoin"></textarea>
            <button type="submit" class="btn btn-block btn-outline-success" name="enregistrer">Enregistrer</button>

        </form>
    </div>
    <?php include 'code_client.php'; ?>
        <script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
    <?php include 'footer.php'; ?>
</body>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</html>