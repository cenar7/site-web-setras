<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--Css local-->
    <link rel="stylesheet" href="stylesheet.css">
    <title>SETRAS CAMEROUN</title>
    <link rel="icon" href="favicon.ico" />
    <link rel="icon" type="image/png" href="Logotype SETRAS.jpg" />
</head>
<body>
<?php include 'header.php'; ?>
<?php include 'Menu.php'; ?>
<img src="img9.jpg" class="d-block w-100" height="400">
<div class="container mt-2 bg-light">
    <p><strong>SETRAS</strong> est une Entreprise d’Ingénierie et de Construction avec une filiale en République de Guinée Equatoriale. L’entreprise est spécialisée dans les domaines qui suivent :</p>
                 
      <ul>
          <li>Conception architecturale et technique des projets,</li>
          <li>L’analyse et le calcul des ouvrages du Génie Civil,</li>
          <li>La réalisation des études Techniques,</li>
          <li>Les études des projets routiers et de terrassement des Plates-formes,</li>
          <li>Les études d’exécution des CET (Corps d’Etat Techniques),</li>
          <li>La réalisation des études d’APS, APD et DCE,</li>
          <li>Constructions Bâtiments & Ouvrages d’Art,</li>
          <li>Le Contrôle et la supervision des travaux,</li>
          <li>L’économie des constructions,</li>
          <li>L’expertise et le conseil.</li>
      </ul>
        
<p>L’entreprise est née du souci de ses promoteurs de répondre aux préoccupations des usagers et des entreprises dans le domaine des Etudes Techniques détaillées, complètes, compétitives et respectant les codes déontologiques des métiers du Bâtiment et des Travaux publics. Et la Construction des Bâtiments et Ouvrages d’Art respectant les normes.</p>
       </br>
       <p>Nous avons le souci de mettre notre expérience et nos compétences acquises dans les études et la réalisation de nombreux projets de grandes tailles en Afrique, au service des particuliers, des entreprises et des Etats.</p>
       </br>
       <p>Nos Ingénieurs sont là pour apporter des solutions efficaces et durables aux nombreux problèmes que pose la construction.</p>
         </br></br></br></br></br></br>
</div>
    <?php include 'footer.php'; ?>
</body>
</html>