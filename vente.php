<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  
    <!--Css local-->
    <link rel="stylesheet" href="stylesheet.css">
    <link rel="stylesheet" href="style.css">
    <title>SETRAS CAMEROUN</title>
    <link rel="icon" href="favicon.ico" />
    <link rel="icon" type="image/png" href="Logotype SETRAS.jpg" />
</head>
<body>
<?php include 'header.php'; ?>
<?php include 'Menu.php'; ?>
<section class="banner_area">
      <div class="banner_inner d-flex align-items-center">
        <div class="container">
          <div class="banner_content d-md-flex justify-content-between align-items-center">
            <div class="mb-3 mb-md-0">
              <h2>Shop Category</h2>
              <p>Ici vous trouverez les produits disponible chez nous</p>
            </div>
            <div class="page_link">
              <a href="Accueil.php">Home</a>
              <a href="vente.php">Shop</a>
            </div>
          </div>
        </div>
      </div>

       <!--================Category Product Area =================-->
    <section class="cat_product_area section_gap">
      <div class="container">
        <div class="row flex-row-reverse">
          <div class="col-lg-9">
            <div class="product_top_bar">
              <h4 style="text-align:center;">QUELQUES PRODUITS</h4>
            </div>

            
            <div class="latest_product_inner">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <div class="product-img">
                                        <img class="card-img" src="v1.jpg" alt="" width="340" height="300" />
                                       
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>digi-Schmild.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b001</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 ">
                                <div class="single-product ">
                                    <div class="product-img bg-light">
                                        <img class="card-img" src="v2.png" alt="" width="340" height="300"/>
                                       
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>Trimble.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b002</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <div class="product-img">
                                        <img class="card-img" src="v3.jpg" alt="" width="340" height="300" />
                                       
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>Profoscope.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b003</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <div class="product-img">
                                        <img class="card-img" src="v4.jpg" alt="" width="340" height="300" />
                                        
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>WC moderne.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b004</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <div class="product-img">
                                        <img class="card-img" src="v5.jpg" alt="" width="340" height="300"/>
                                        
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>Betoneuse.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b005</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <div class="product-img">
                                        <img class="card-img" src="v6.jpg" alt="" width="340" height="300" />
                                        
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>Carreaux.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b006</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <div class="product-img">
                                        <img class="card-img" src="v7.jpg" alt="" width="340" height="300"/>
                                       
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>Seau de peinture.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b007</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <div class="product-img">
                                        <img class="card-img" src="v8.jpg" alt="" width="340" height="300" />
                                       
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>Faux plancher.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b008</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <div class="product-img">
                                        <img class="card-img" src="v9.jpg" alt="" width="340" height="300"/>
                                        
                                    </div>
                                    <div class="product-btm">
                                        <a href="facturation.php" class="d-block">
                                            <h4>Tuyaux pour plomberie.</h4>
                                        </a>
                                        <div class="mt-3">
                                            <span class="mr-4">.......FCFA</span>
                                            <del>.......FCFA</del>
                                            <p>ref:b009</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
        </div>
      </div>
    </section>
</section>
 <?php include 'footer.php'; ?>
</body>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>    
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>