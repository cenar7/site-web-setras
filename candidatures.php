<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--Css local-->
    <link rel="stylesheet" href="stylesheet.css">

    <title>SETRAS CAMEROUN</title>
    <link rel="icon" href="favicon.ico" />
    <link rel="icon" type="image/png" href="Logotype SETRAS.jpg" />
    </head>
<body>
<?php include 'header.php'; ?> 
<?php include 'Menu.php'; ?>
<img src="img5.jpg" class="d-block w-100" height="400">
<div class="container mt-3 bg-light">
        <p>Vous souhaitez nous rejoindre ? Laissez-nous votre CV et Lettre de Motivation. Nos équipes se chargeront de les étudier et de vous répondre.</p>
        <form action="" method="post" class="needs-validation" novalidate enctype="multipart/form-data" >
            <div class="form-group">
                <input type="text" class="form-control" id="uname" placeholder="Email" name="Email" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <div class="custom-file mb-3">
            <p><strong>CV (pdf ou word max 2M)</strong></p>
            <input type="file" name="cv">
            </div>
            <div class="custom-file mb-3">
            <p><strong>Lettre de Motivation (pdf ou word max 2M)</strong></p>
            <input type="file"  name="lettre">
            </div>
            <button type="submit" class="btn btn-block btn-outline-success" name="envoyer">Envoyer</button>
        </form>
</div>
<?php include 'code_candidature.php'; ?>
        <script>
    // Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
</div>
                
            
<?php include 'footer.php'; ?>
</body>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</html>