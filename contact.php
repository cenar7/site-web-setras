<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!--Css local-->
    <link rel="stylesheet" href="stylesheet.css">

    <title>SETRAS CAMEROUN</title>
    <link rel="icon" href="favicon.ico" />
    <link rel="icon" type="image/png" href="Logotype SETRAS.jpg" />

    
    
</head>
<body>
<?php include 'header.php'; ?>
<?php include 'Menu.php'; ?>
<img src="img6.jpg" class="d-block w-100" height="400">
<div class="container-fluid bg-light">
 <div class="row mt-0">
  <div class="col-sm-6 mt-5 bg-light">
         <p> <strong>SETRAS</strong> offre des stages académiques et pré-emploi aux jeunes étudiants de l’enseignement supérieur. Remplissez les cases ci-dessous et nous vous contacterons. </p>
         <form action="" method="post" class="needs-validation" novalidate>
            <div class="form-group">
                <input type="text" class="form-control" id="uname" placeholder="Nom(s) et prénom(s)" name="Nom" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="uname" placeholder="Adresse e-mail" name="Email" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="uname" placeholder="Objet du méssage" name="Message" required>
                <div class="valid-feedback">Valide.</div>
                <div class="invalid-feedback">Veuillez remplir ce champ.</div>
            </div>
            <textarea class="form-control" rows="10" id="comment" placeholder="Entrer votre besoin ici" name="Besoin"></textarea>
            <button type="submit" class="btn btn-block btn-outline-success" name="enregistrer">Enregistrer</button>
         </form>
         <?php include 'code_visiteur.php'; ?>
      
        <script>
       // Disable form submissions if there are invalid fields
       (function() {
       'use strict';
       window.addEventListener('load', function() {
       // Get the forms we want to add validation styles to
       var forms = document.getElementsByClassName('needs-validation');
       // Loop over them and prevent submission
       var validation = Array.prototype.filter.call(forms, function(form) {
       form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
       }, false);
       });
       }, false);
       })();
      </script>
      
    </div>
    
<div class="col-sm-6 mt-5 bg-light">
<div class="row mt-0">
<div class="col-sm  bg-light">
      <h5>Adresses sociales</h5>
       Bonamoussadi </br>
       Douala-Cameroun </br>
       Boite postale : 15681 </br></br>
       <h5>Mails</h5>
       setras@yahoo.fr </br></br>
     </div>
     <div class="col-sm-3 bg-light">
     <h5>Téléphones </h5>
   (+237)699 94 63 31 </br> (+237)233 33 02 38 15
    </div>
</div>
</div>
 </div>
</div>

</body>
</html>








<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</html>